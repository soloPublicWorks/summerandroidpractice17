package ru.geekbrains.android.level1.handlertimer;

/**
 * Created by Валера on 27.08.2017.
 */

public interface ReportProgress {
    void showProgress(int progress);
}
