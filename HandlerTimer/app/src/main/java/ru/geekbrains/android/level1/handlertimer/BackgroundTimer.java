package ru.geekbrains.android.level1.handlertimer;

import android.os.AsyncTask;

/**
 * Created by Валера on 27.08.2017.
 */

public class BackgroundTimer extends AsyncTask<Void, Integer, Void> {
    private int n;
    private boolean onStop;
    private ReportProgress reportProgress;
    private boolean needProgress;

    public BackgroundTimer(int n, ReportProgress reportProgress) {
        this.n = n;
        onStop = true;
        this.reportProgress = reportProgress;
        needProgress = true;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        do {
            publishProgress(n);
            try {
                Thread.sleep(1000);
                ++n;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (onStop);
        return null;
    }
    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if (needProgress) reportProgress.showProgress(values);
    }

    public synchronized int onStop() {
        onStop = false;
        return n;
    }
    public synchronized void unlink() {
        reportProgress = null;
        needProgress = false;
    }
    public synchronized void link(ReportProgress reportProgress) {
        this.reportProgress = reportProgress;
        needProgress = true;
    }
}
