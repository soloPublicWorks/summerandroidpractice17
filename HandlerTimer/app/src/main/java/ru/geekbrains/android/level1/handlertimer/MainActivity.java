package ru.geekbrains.android.level1.handlertimer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements ReportProgress {
    private TextView tvTime;
    private Button btnStart;

    private BackgroundTimer backgroundTimer;
    private boolean isBackRunning;
    private int backSeconds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvTime = (TextView) findViewById(R.id.textView);
        btnStart = (Button) findViewById(R.id.button);

        backSeconds = 0;
        isBackRunning = false;

        //backgroundTimer = (BackgroundTimer) getLastNonConfigurationInstance();
        if (backgroundTimer != null) {
            backgroundTimer.link(this);
        }
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        if (backgroundTimer == null) return null;
        backgroundTimer.unlink();
        return backgroundTimer;
    }

    @Override
    public void showProgress(int progress) {
        tvTime.setText(String.valueOf(progress));
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button:
                if (backgroundTimer == null || !isBackRunning) {
                    backgroundTimer = (BackgroundTimer) getLastNonConfigurationInstance();
                    if (backgroundTimer == null) backgroundTimer = new BackgroundTimer(backSeconds, this);
                    else backgroundTimer.link(this);
                    backgroundTimer.execute();
                    isBackRunning = true;
                } else {
                    backgroundTimer.onStop();
                    isBackRunning = false;
                }
        }
    }
}
