package com.hfad.messenger;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class CreateMessageActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_message);
    }

    public void onClickSendMessage(View view) {
        EditText editText = (EditText) findViewById(R.id.editText);
        String messageText = editText.getText().toString();

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        /* установка типа MIME(Multipurpose Internet Mail Extension(многоцелевые расширения интернет-почты)
           — стандарт, описывающий передачу различных типов данных по электронной почте, а также,
           в общем случае, спецификация для кодирования информации и форматирования сообщений
           таким образом, чтобы их можно было пересылать по Интернету.
         */
        intent.putExtra(Intent.EXTRA_TEXT, messageText);

        String chooserTitle = getString(R.string.chooser);
        Intent chosenIntent = Intent.createChooser(intent, chooserTitle);
        startActivity(chosenIntent);
    }
}
