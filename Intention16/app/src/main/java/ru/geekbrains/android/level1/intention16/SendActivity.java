package ru.geekbrains.android.level1.intention16;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SendActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String EMPTY_STRING = "";

    private EditText etMessage;
    private TextView btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send);

        initViews();
    }

    private void initViews() {
        etMessage = (EditText) findViewById(R.id.et_message);
        btnSend = (Button) findViewById(R.id.btn_send);
        btnSend.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (!etMessage.getText().toString().equals(EMPTY_STRING)) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, etMessage.getText().toString());
            intent.setType("text/plain");
            Intent chosenIntent = Intent.createChooser(intent, getString(R.string.send_by));
            startActivity(chosenIntent);
        } else
            Toast.makeText(this, getString(R.string.wrong_input), Toast.LENGTH_SHORT)
                    .show();
    }
}

