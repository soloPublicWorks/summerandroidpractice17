package ru.level1.android.geekbrains.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivityFirstOption extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
