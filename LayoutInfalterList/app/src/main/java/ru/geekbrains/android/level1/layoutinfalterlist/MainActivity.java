package ru.geekbrains.android.level1.layoutinfalterlist;

import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    private String[] name = { "Иван", "Марья", "Петр", "Антон", "Даша", "Борис",
            "Костя", "Игорь"};
    private String[] position = { "Программер", "Бухгалтер", "Программер",
            "Программер", "Бухгалтер", "Директор", "Программер", "Охранник" };
    private int salary[] = { 13000, 10000, 13000, 13000, 10000, 15000, 13000, 8000 };
    private int[] colors = new int[2];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        colors[0] = Color.parseColor("#559966CC");
        colors[1] = Color.parseColor("#55336699");

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linLayout);
        LayoutInflater layoutInflater = getLayoutInflater();

        for (int i = 0; i < name.length; i++) {
            View item = layoutInflater.inflate(R.layout.item, linearLayout, false);
            TextView tvName = item.findViewById(R.id.tvName);
            tvName.setText(name[i]);
            TextView tvPosition = item.findViewById(R.id.tvPosition);
            tvPosition.setText("Должность: " + position[i]);
            TextView tvSalary = item.findViewById(R.id.tvSalary);
            tvSalary.setText("Зарплата: " + salary[i]);
            item.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
            item.setBackgroundColor(colors[i % 2]);
            linearLayout.addView(item);
        }
    }
}
