package ru.geekbrains.android.level1.intention;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SendActivity extends AppCompatActivity {
    public static final String EXTRA_KEY = "extraKey";
    private EditText etMessage;
    private TextView btnSend;

    public EditText getEtMessage() {
        return etMessage;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send);

        initViews();
    }

    private void initViews() {
        etMessage = (EditText) findViewById(R.id.et_message);
        btnSend = (Button) findViewById(R.id.btn_send);
        btnSend.setOnClickListener(new OnClickSend(this));

    }
}

class OnClickSend implements View.OnClickListener {
    private AppCompatActivity fromActivity;

    public OnClickSend(AppCompatActivity fromActivity) {
        this.fromActivity = fromActivity;
    }

    @Override
    public void onClick(View view) {
        if (!((SendActivity) fromActivity).getEtMessage().getText().equals("")) {
            Intent intent = new Intent(fromActivity, ReceiveActivity.class);
            intent.putExtra(((SendActivity) fromActivity).EXTRA_KEY,
                    ((SendActivity) fromActivity).getEtMessage().getText());
            fromActivity.startActivity(intent);
        } else
            Toast.makeText(fromActivity, fromActivity.getString(R.string.wrong_input), Toast.LENGTH_SHORT)
                    .show();
    }
}
