package ru.geekbrains.android.level1.intention;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ReceiveActivity extends AppCompatActivity {
    private TextView tvReceivedMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive);

        initViews();
        getDataFromIntent();
    }

    private void initViews() {
        tvReceivedMessage = (TextView) findViewById(R.id.tv_received_message);
    }

    public void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            tvReceivedMessage.setText(intent.getStringExtra(SendActivity.EXTRA_KEY));
        }
    }
}
