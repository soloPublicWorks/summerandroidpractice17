package com.hfad.beeradviser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Валера on 18.08.2017.
 */

public class BeerExpert {
    public static String[] LIGHT_BRANDS = {"Jack Amber", "Red Moose"};
    public static String[] OTHER_BRANDS = {"Jail Pale Ale", "Gout Stout"};

    public static List<String> getBrands(String beerColor){
        List<String> brands = new ArrayList<>();
        switch (beerColor) {
            case "light":
                brands.addAll(Arrays.asList(LIGHT_BRANDS));
                break;
            default:
                brands.addAll(Arrays.asList(OTHER_BRANDS));
                break;
        }
        return brands;
    }
}
