package com.hfad.beeradviser;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

public class FindBeerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_beer);
    }

    public void onClickFindBeer(View view) {
        Spinner spinnerColor = (Spinner) findViewById(R.id.spinner_beer_color);
        TextView tvBrands = (TextView) findViewById(R.id.textView_brands);

        String beerColor = spinnerColor.getSelectedItem().toString();
        List<String> brands = BeerExpert.getBrands(beerColor);

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < brands.size(); i++) {
            if (i == brands.size() - 1) stringBuilder.append(brands.get(i)).append(".");
            else stringBuilder.append(brands.get(i)).append(", ");
        }

        tvBrands.setText(stringBuilder);
    }
}
