package ru.geekbrains.android.level1.timer;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TimerActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int SLEEP_TIME = 1000;
    public static final String KEY_SECONDS = "seconds";
    public static final String KEY_RUNNING = "running";

    private TextView tvTime;

    private int seconds;
    private boolean running;
    private boolean wasRunning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

        if (savedInstanceState != null) {
            seconds = savedInstanceState.getInt(KEY_SECONDS);
            running = savedInstanceState.getBoolean(KEY_RUNNING);
        } else {
            seconds = 0;
            running = false;
        }

        initViews();

        runTimer();
    }

    @Override
    protected void onStart() {
        running = wasRunning;
        super.onStart();
    }

    @Override
    protected void onStop() {
        wasRunning = running;
        running = false;
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(KEY_SECONDS, seconds);
        outState.putBoolean(KEY_RUNNING, running);
    }

    private void initViews() {
        tvTime = (TextView) findViewById(R.id.tv_time);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_start:
                running = true;
                break;
            case R.id.btn_stop:
                running = false;
                break;
            case R.id.btn_reset:
                seconds = 0;
                running = false;
                break;
        }
    }

    private void runTimer() {
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(this, SLEEP_TIME);
                int hours = seconds / 3600;
                int minutes = seconds / 60;
                int sec = seconds % 60;
                String time = String.format("%d:%02d:%02d", hours, minutes, sec);

                if (running) seconds++;
                tvTime.setText(time);
            }
        });
    }
}
