package ru.geekbrains.android.level1.sendemail;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.IOException;

public class BlankActivity extends AppCompatActivity {
    private static final String MIME_TYPE_IMAGE = "image/*";
    private static final String MIME_TYPE_TEXT = "text/plain";
    private static final int PICK_CODE = 1;

    private EditText etAddress;
    private EditText etSubject;
    private EditText etMessage;
    private ImageView image;
    private Uri uriImage;
    private Button btnPick;
    private Button btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blank);

        initViews();
    }

    private void initViews() {
        etAddress = (EditText) findViewById(R.id.et_address);
        etSubject = (EditText) findViewById(R.id.et_subject);
        etMessage = (EditText) findViewById(R.id.et_message);

        image = (ImageView) findViewById(R.id.image);
        btnPick = (Button) findViewById(R.id.btn_pick);
        btnPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType(MIME_TYPE_IMAGE);
                startActivityForResult(intent, PICK_CODE);
            }
        });

        btnSend = (Button) findViewById(R.id.btn_send);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etAddress.getText().toString().equals("") &&
                        !etSubject.getText().toString().equals("") &&
                        !etMessage.getText().toString().equals("")) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_EMAIL, etAddress.getText().toString());
                    intent.putExtra(Intent.EXTRA_SUBJECT, etSubject.getText().toString());
                    intent.putExtra(Intent.EXTRA_TEXT, etMessage.getText().toString());
                    intent.setType(MIME_TYPE_TEXT);
                    intent.putExtra(Intent.EXTRA_STREAM, uriImage);
                    intent.setType(MIME_TYPE_IMAGE);

                    startActivity(Intent.createChooser(intent, getString(R.string.send_by)));
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == PICK_CODE) {
            Bitmap bitmap;
            uriImage = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uriImage);
                image.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        btnPick.setVisibility(View.GONE);
    }
}
