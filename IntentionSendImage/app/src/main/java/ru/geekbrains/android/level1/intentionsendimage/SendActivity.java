package ru.geekbrains.android.level1.intentionsendimage;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore.Images.Media;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.IOException;
import java.net.URI;

public class SendActivity extends AppCompatActivity implements View.OnClickListener{
    private static final int REQUEST_CODE = 1;
    private static final String MIME_TYPE = "image/*";

    private ImageView imageView;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send);

        initViews();
    }

    private void initViews() {
        imageView = (ImageView) findViewById(R.id.iv);
        button = (Button) findViewById(R.id.btn);
        button.setOnClickListener(this);
    }

    private void changeButton() {
        button.setText(getString(R.string.send_image));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType(MIME_TYPE);
                intent.putExtra(MIME_TYPE, (Parcelable) imageView.getDrawable());
                startActivity(Intent.createChooser(intent, getString(R.string.send_by)));
            }
        });
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MIME_TYPE);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
            Uri selectedImage = data.getData();
            try {
                Bitmap image = Media.getBitmap(getContentResolver(), selectedImage);
                imageView.setImageBitmap(image);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        changeButton();
    }
}
