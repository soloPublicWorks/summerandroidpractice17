package ru.geekbrains.android.level1.startactivityforresult;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class InfoActivity extends AppCompatActivity implements View.OnClickListener{
    public static final int REQUEST_CODE = 1;

    private TextView tvName;
    private TextView tvPhone;
    private Button btnFillBlank;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        initView();
    }

    private void initView() {
        tvName = (TextView) findViewById(R.id.tv_name);
        tvPhone = (TextView) findViewById(R.id.tv_phone);
        btnFillBlank = (Button) findViewById(R.id.btn_fill_blank);
        btnFillBlank.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, BlankActivity.class);
        startActivityForResult(intent, REQUEST_CODE);   // запуск операции, возвращающей результат
    }

    // метод приёма данных из запущенного этой операцией операции
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            tvName.setText(data.getStringExtra(BlankActivity.KEY_NAME));
            tvPhone.setText(data.getStringExtra(BlankActivity.KEY_PHONE));
        }
    }
}
