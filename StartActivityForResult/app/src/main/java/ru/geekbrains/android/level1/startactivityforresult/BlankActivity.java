package ru.geekbrains.android.level1.startactivityforresult;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class BlankActivity extends AppCompatActivity implements View.OnClickListener{
    public static final String KEY_NAME = "name";
    public static final String KEY_PHONE = "phone";

    private EditText etName;
    private EditText etPhone;
    private Button btnFill;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blank);

        initViews();
    }

    private void initViews() {
        etName = (EditText) findViewById(R.id.et_name);
        etPhone = (EditText) findViewById(R.id.et_phone);
        btnFill = (Button) findViewById(R.id.btn_fill);
        btnFill.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent();   // намерение без адресата
        intent.putExtra(KEY_NAME, etName.getText().toString());
        intent.putExtra(KEY_PHONE, etPhone.getText().toString());
        setResult(RESULT_OK, intent);   // метод указывает на операцию из которой произошёл вызов
        finish();   // завершение работы операции
    }
}
