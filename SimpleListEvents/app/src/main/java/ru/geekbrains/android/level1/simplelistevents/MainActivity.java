package ru.geekbrains.android.level1.simplelistevents;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,
                                                                AdapterView.OnItemSelectedListener,
                                                                AbsListView.OnScrollListener{
    private ListView lvMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvMain = (ListView) findViewById(R.id.lvMain);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.names, android.R.layout.simple_list_item_1);
        lvMain.setAdapter(adapter);

        lvMain.setOnItemClickListener(this);
        lvMain.setOnItemSelectedListener(this);
        lvMain.setOnScrollListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(this, "Item clicked: " + ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(this, "Item selected: " + ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        Toast.makeText(this, "Nothing selected", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {
        switch (i) {
            case SCROLL_STATE_IDLE:
                Toast.makeText(this, "List scrolling ended", Toast.LENGTH_SHORT).show();
                break;
            case SCROLL_STATE_FLING:
                Toast.makeText(this, "List is flinging", Toast.LENGTH_SHORT).show();
                break;
            case SCROLL_STATE_TOUCH_SCROLL:
                Toast.makeText(this, "List has started scrolling", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onScroll(AbsListView absListView, int i, int i1, int i2) {
        Toast.makeText(this, "scroll: firstVisibleItem = " + i
                + ", visibleItemCount" + i1
                + ", totalItemCount" + i2 , Toast.LENGTH_SHORT).show();
    }
}
