package ru.geekbrains.android.level1.simplelistchoice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private ListView lvMain;
    private String[] names;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvMain = (ListView) findViewById(R.id.lvMain);
        // режим выбора пунктов меню
        lvMain.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter
                .createFromResource(this, R.array.names, android.R.layout.simple_list_item_multiple_choice);
        lvMain.setAdapter(adapter);

        names = getResources().getStringArray(R.array.names);
        Button btnChecked = (Button) findViewById(R.id.btnChecked);
        btnChecked.setOnClickListener(this);
    }

    public void onClick(View view) {
        SparseBooleanArray array = lvMain.getCheckedItemPositions();
        for (int i = 0; i < array.size(); i++) {
            int key = array.keyAt(i);
            if (array.get(key))
                Toast.makeText(this, names[key], Toast.LENGTH_SHORT).show();
        }

    }
}
