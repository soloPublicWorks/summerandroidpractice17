package ru.geekbrains.android.level1.lesson2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Spinner spinner;
    TextView tvDescription;
    Description description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    private void initViews() {
        description = new Description(this);
        //ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.list_layout, R.id.textView, description.getItems());
        MySpinnerAdapter mySpinnerAdapter = new MySpinnerAdapter(description);

        spinner = (Spinner) findViewById(R.id.spnr_colors);
        spinner.setAdapter(mySpinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                onClick(null);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        tvDescription = (TextView) findViewById(R.id.tv_description);

        onClick(null);
    }

    @Override
    public void onClick(View view) {
        tvDescription.setText(description.getEffect(spinner.getSelectedItem().toString()));
    }
}
