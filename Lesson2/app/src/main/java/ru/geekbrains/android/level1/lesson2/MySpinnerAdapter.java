package ru.geekbrains.android.level1.lesson2;

import android.database.DataSetObserver;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by Валера on 20.08.2017.
 */

public class MySpinnerAdapter implements SpinnerAdapter {
    Description description;

    public MySpinnerAdapter(Description description) {
        this.description = description;
    }

    @Override
    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_layout, null);
        }
        ImageView imageView = view.findViewById(R.id.imageView);
        TextView textViewList = view.findViewById(R.id.tv_list);
        GradientDrawable gradientDrawable = (GradientDrawable) imageView.getDrawable();
        gradientDrawable
                .setColor(ContextCompat
                        .getColor(view.getContext(), description.getColor(description.getItems()[i])));
        textViewList.setText(description.getItems()[i]);
        return view;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.spinner_layout, null);
        }
        TextView textViewList = view.findViewById(R.id.tv_selected);
        textViewList.setText(description.getItems()[i]);
        return view;
    }

    @Override
    public int getCount() {
        return description.getItems().length;
    }

    @Override
    public Object getItem(int i) {
        return description.getItems()[i];
    }

    @Override
    public long getItemId(int i) {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public int getItemViewType(int i) {
        return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {

    }
    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

    }
}
