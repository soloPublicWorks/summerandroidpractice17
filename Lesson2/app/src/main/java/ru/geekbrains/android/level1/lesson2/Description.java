package ru.geekbrains.android.level1.lesson2;

import android.content.Context;

/**
 * Created by vbm on 28/02/2017.
 */


public class Description {
    String[] items ;
    Context context;

    public Description (Context context) {
        this.context = context;
        items = context.getResources().getStringArray(R.array.array_colors);
    }


    public String  getEffect ( String  color )   {
        String  effect;

        if   ( color . equals ( items[0] ))   {
            effect  =  context.getResources().getString(R.string.description_red);
        }   else
        if   ( color . equals ( items[1] ))   {
            effect  =  context.getResources().getString(R.string.description_orange);
        }   else
        if   ( color . equals ( items[2] ))   {
            effect  =  context.getResources().getString(R.string.description_yellow);
        }   else
        if   ( color . equals ( items[3] ))   {
            effect  =  context.getResources().getString(R.string.description_green);
        }   else
        if   ( color . equals ( items[4] ))   {
            effect  =  context.getResources().getString(R.string.description_cyan);
        }   else
        if   ( color . equals ( items[5] ))   {
            effect  =  context.getResources().getString(R.string.description_blue);
        } else   {
            effect  =  context.getResources().getString(R.string.description_purple);
        } return  effect ;
    }

    public int getColor( String color ){
        if( color.equals(items[0])) return R.color.colorRed;
        if( color.equals(items[1])) return R.color.colorMagenta;
        if( color.equals(items[2])) return R.color.colorYellow;
        if( color.equals(items[3])) return R.color.colorGreen;
        if( color.equals(items[4])) return R.color.colorLightBlue;
        if( color.equals(items[5])) return R.color.colorBlue;
        if( color.equals(items[6])) return R.color.colorViolet;
        return 0;
    }

    public String[] getItems() { return items; }
}
